<img src="https://codeberg.org/repo-avatars/28179-90a9ca0e176bc91375d90b3e0b1a11d3" alt="Ícone" width="80px">

# CRUA

Um programa de brinquedo que estou usando apenas para estudo.

O interpretador [Lua](https://lua.org) propriamente é real
(versão [5.4](https://www.lua.org/versions.html#5.4)), diga-se de
passagem. Esse programa é apenas uma casca que usa a biblioteca
[editline](https://github.com/troglobit/editline) para receber os
comandos, passar para Lua via API C, e ao mesmo tempo manter um
histórico temporário.

Caso queira saber como funciona um interpretador de verdade, ignore
este projeto e veja o [interpretador Lua
original](https://github.com/lua/lua/blob/master/lua.c).

## Dependências

* **Compilação**:
  * make
  * pkg-config
  * lua
  * editline

* **Execução**:
  * lua
  * editline

### Instruções de compilação

```
make
```

## Licença

Este programa está disponível sob a licença MIT, vide 
[LICENSE](LICENSE).
