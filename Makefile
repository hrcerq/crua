INCFLAGS != pkg-config --cflags lua5.4
CFLAGS = -Wall -Werror -lm -ldl $(INCFLAGS)
LDFLAGS != pkg-config --libs-only-l lua5.4 libeditline

all : crua

crua : crua.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@  $^

clean :
	rm crua

.PHONY : all clean

