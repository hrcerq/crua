/*
   CRUA
  
   Um programa de brinquedo, que uso apenas para aprendizado.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "editline.h"

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#define TRECHO "crua"

int main (void)
{

    char *buffer;

    lua_State *L = luaL_newstate ();
    luaL_openlibs (L);
    
    puts("CRUA - um interpretador Lua, só que mais cru ...");

    while ((buffer = readline(":: ")))
    {

        if (
            luaL_loadbuffer(L, buffer, strlen (buffer), TRECHO) != LUA_OK
            || lua_pcall (L, 0, 0, 0) != LUA_OK
        )
        {
            // erros
            fprintf (stderr, "%s\n", lua_tostring (L, -1));
            lua_pop (L, -1);
        }

        free(buffer);
    }
    lua_close (L);

    puts("Até mais...");
    return EXIT_SUCCESS;
}
